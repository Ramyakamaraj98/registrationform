form.addEventListener('submit',(e)=>{
e.preventDefault();
validateInputs();
})
function validateInputs(){
    const name=document.querySelector('#Name');
    const Father_Name=document.querySelector('#Father_Name');
    const Mobile_No=document.querySelector('#Mobile_No');
    const Email=document.querySelector('#Email');
    const Gender=document.querySelector('#Gender');
    const Date_Of_Birth=document.querySelector('#Date_Of_Birth');
    const Nameval=Name.value.trim();
    const Father_Nameval=Father_Name.value.trim();
    const Mobile_Noval=Mobile_No.value.trim();
    const Emailval=Email.value.trim();
    const DateVal=Date_Of_Birth.value;
    const genderval=Genderchk();

    if(!genderval)
    {
        setError("genderError",'gender is required');
    }
    else{
        setSuccess("genderError");
    }

    if(Nameval===''){
        setError("nameError",'gender is required');
    }
    else if(Nameval.length<3){
        setError("nameError",'name should be more than 3 character');
    }
    else{
        setSuccess("nameError");
    }

    if(Father_Nameval===''){
        setError("fNameError",'Father name is requires');
    }
    else if(Father_Nameval.length<3){
        setError("fNameError",'name should be more than 3 character');
    }
    else{
        setSuccess("fNameError");
    }

    if(Mobile_Noval===""){
        setError("MobilenoError",'mobile_no is required');
    }
    else if(isNaN(Mobile_Noval)){
        setError("MobilenoError",'mobile_no should be a number');
    }
    else if(!mobileValidation(Mobile_Noval[0])){
        setError("MobilenoError",'mobile_no should be start with 9 or 8 or 7');
    }
    else if(Mobile_Noval.length>10||Mobile_Noval.length<10){
        setError("MobilenoError",'mobile_no should be 10 number');
    }
    else{
        setSuccess("MobilenoError");
    }

    if(Emailval===""){
        setError("emailError",'Email is required');
    }
    else{
        setSuccess("emailError");
    }
    
    if(DateVal==""){
        setError("dateOfBirthError",'Date_Of_Birth is required');
    }
    else{
        setSuccess("dateOfBirthError");
    }
}

function setError(id,message){
    const error=document.getElementById(id);
    error.innerHTML=message;
    error.classList.add('error');
}

function setSuccess(id){
    const error=document.getElementById(id);
    error.innerHTML="";
    error.classList.remove('error');
}

function Genderchk(){
    const a = [];
    let b;
    document.getElementsByName("Gender").forEach((e,i)=>{
        a[i] = {
            value: e.value,
            checked: e.checked
        };
    });
     a.forEach(e=>{
        if(e.checked){
            b= e.value;
        }
    });
    return b;
}

function mobileValidation(x){
    switch (x) {
        case '9':
            return true;
            case '8':
            return true;
            case '7':
                return true;
        default:
            return false;
    }
}